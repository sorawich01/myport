// state
export const state = {
	Number : 0
}

// getters
export const getters = {
	counter(state){
		return state.Number
	}
}

// mutations
export const mutations = {
	decresementCounter (state, payload){
		state.Number += payload
	}
}

// actions
export const actions = {
	decresementAction ( {commit} , payload) {
		commit('decresementCounter', payload)
	}
}