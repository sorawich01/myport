import axios from 'axios'
import * as types from '../mutation-types'

const url = '/api/aaa'

// state
export const state = {
  aaa: null
}

// getters
export const getters = {
  aaa: state => state.aaa
}

// mutations
export const mutations = {
  [types.FETCH_AAA] (state, { aaa }) {
    state.aaa = aaa
  }
}

// actions
export const actions = {
  async fetchHistorys ({ commit }) {
    try {
      const { data } = await axios.get(url)
      commit(types.FETCH_AAA, { aaa: data })
    } catch (e) {
      console.warn(e)
    }
  }
}
