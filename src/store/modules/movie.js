import axios from 'axios'
import * as types from '../mutation-types'

const url = '/api/movies'

// state
export const state = {
  movies: null
}

// getters
export const getters = {
  movies: state => state.movies
}

// mutations
export const mutations = {
  [types.FETCH_MOVIE] (state, { movie }) {
    state.movies = movie
  }
}

// actions
export const actions = {
  async fetchMovies ({ commit }) {
    try {
      const { data } = await axios.get(url)
      commit(types.FETCH_MOVIE, { movie: data })
    } catch (e) {
      console.warn(e)
    }
  },
  async fetchMovieById ({ commit }, id) {
    try {
      const { data } = await axios.get(url + '/' + id)
      commit(types.FETCH_MOVIE, { movie: data })
    } catch (e) {
    }
  },
  async add_movie ({ dispatch }, data) {
    await axios.post(url, data)
    dispatch('movie/fetchMovies', null, { root: true })
  },
  async del_movie ({ dispatch }, id) {
    await axios.delete(`${url}/${id}`)
    dispatch('movie/fetchMovies', null, { root: true })
  }
}
