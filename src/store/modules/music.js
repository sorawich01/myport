import axios from 'axios'
import * as types from '../mutation-types'

const url = '/api/musics'

// state
export const state = {
  musics: null
}

// getters
export const getters = {
  musics: state => state.musics
}

// mutations
export const mutations = {
  [types.FETCH_MUSIC] (state, { music }) {
    state.musics = music
  }
}

// actions
export const actions = {
  async fetchMusics ({ commit }) {
    try {
      const { data } = await axios.get(url)
      commit(types.FETCH_MUSIC, { music: data })
    } catch (e) {
      console.warn(e)
    }
  },
  async fetchMusicById ({ commit }, id) {
    try {
      const { data } = await axios.get(url + '/' + id)
      commit(types.FETCH_MUSIC, { music: data })
    } catch (e) {
      console.warn(e)
    }
  },
  async add_music ({ dispatch }, data) {
    await axios.post(url, data)
    dispatch('music/fetchMusics', null, { root: true })
  },
  async update_music ({ dispatch }, data) {
    await axios.put(`${url}/${data.id}`, data)
    dispatch('music/fetchMusics', null, { root: true })
  },
  async del_music ({ dispatch }, id) {
    await axios.delete(`${url}/${id}`)
    dispatch('music/fetchMusics', null, { root: true })
  }
}
