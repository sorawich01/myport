import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router)

export default new Router({
	scrollBehavior() {
    return { x: 0, y: 0 };
  },
	routes:[
		{
			name : 'Static_page',
			path : '/',
			component: () => import('../pages/static.vue'),
		},
		// {
		// 	name : 'Home_page',
		// 	path : '/homepage',
		// 	component: () => import("../pages/page/HomePage.vue"),

		// },
		// {
		// 	name : 'Calculator_page',
		// 	path : '/calculator',
		// 	component: () => import("../pages/works/calculator.vue"),

		// },
		// {
		// 	name : 'About_page',
		// 	path : '/about',
		// 	component: () => import("../pages/page/about.vue"),

		// },
		// {
		// 	name : 'Work_page',
		// 	path : '/work',
		// 	component: () => import("../pages/page/work.vue")
		// },
		// {
		// 	name : 'Block_Chain_page',
		// 	path : '/blockchain',
		// 	component: () => import("../pages/works/blockchain.vue"),
		// 	meta: {
		// 		reload: true,
		// 	}
		// },
		// {
		// 	name : 'CRUD_page',
		// 	path : '/crud',
		// 	component: () => import('../pages/works/CRUD.vue')
		// },
		{
			name : 'Not_Found_Pages',
			path : '*',
			component : () => import('../pages/Notfoundpage.vue')
		}
		// {
		// 	name : 'Counter_page',
		// 	path : '/counter',
		// 	component : () => import('../pages/works/counter.vue')
		// },
	]
})